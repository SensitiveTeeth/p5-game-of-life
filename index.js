const unitLength  = 20;
const boxColor    = 150;
const strokeColor = 50;
let columns; /* To be determined by window width */
let rows;    /* To be determined by window height */
let currentBoard;
let nextBoard;
let fps = 50
let Loneliness = 2
let Overpopulation = 3
let Reproduction = 3
const settingWidth = 10
const settingHeight = 170
function setup(){
    /* Set the canvas to be under the element #canvas*/
    const canvas = createCanvas(windowWidth - settingWidth, windowHeight - settingHeight);
    frameRate(fps)
    canvas.parent(document.querySelector('#canvas'));

    /*Calculate the number of columns and rows */
    columns = floor(width  / unitLength);
    rows    = floor(height / unitLength);

    /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
    currentBoard = [];
    nextBoard = [];
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    // Now both currentBoard and nextBoard are array of array of undefined values.
    init();  // Set the initial values of the currentBoard and nextBoard
}

function  init() {
    for (let i = 0; i < columns; i++) {
        currentBoard[i] = [];
        nextBoard[i] = []
    }
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = 0;
            nextBoard[i][j] = 0;
        }
    }
}

function draw() {
    background(255);
    generate();
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            if (currentBoard[i][j] == 1 && nextBoard[i][j] == 1) {
                fill("#E11072");//old live
            } else if (nextBoard[i][j] == 1 && currentBoard[i][j] == 0) {
                fill("#F0ACCC")  //currentBoard death change color
            } else {
                fill("#E11072");//old live
                fill(255);
            } 
            stroke(strokeColor);
            rect(i * unitLength, j * unitLength, unitLength, unitLength);
        }
    }
}

function generate() {
    //Loop over every single box on the board
    for (let x = 0; x < columns; x++) {
        for (let y = 0; y < rows; y++) {
            // Count all living members in the Moore neighborhood(8 boxes surrounding)
            let neighbors = 0;
            for (let i of [-1, 0, 1]) {
                for (let j of [-1, 0, 1]) {
                    if( i == 0 && j == 0 ){
                        // the cell itself is not its own neighbor
                        continue;
                    }
                    // The modulo operator is crucial for wrapping on the edge
                    neighbors += currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
                }
            }

            // Rules of Life
            if (currentBoard[x][y] == 1 && neighbors < Loneliness) {
                // Die of Loneliness
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 1 && neighbors > Overpopulation) {
                // Die of Overpopulation
                nextBoard[x][y] = 0;
            } else if (currentBoard[x][y] == 0 && neighbors == Reproduction) {
                // New life due to Reproduction
                nextBoard[x][y] = 1;
                nextBoard[x][y] = 1;
            } else {
                // Stasis
                nextBoard[x][y] = currentBoard[x][y];
            }
        }
    }

    // Swap the nextBoard to be the current Board
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
}

/**
 * When mouse is pressed
 */
function mousePressed() {
    noLoop();
    mouseDragged();
}

/**
 * When mouse is released
 */
// function mouseReleased() {
//     loop();
// }
function nextFrame() {
    loop()
    noLoop()
}

function mouseDragged() {
    /**
     * If the mouse coordinate is outside the board
     */
    if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
        return;
    }
    const x = Math.floor(mouseX / unitLength);
    const y = Math.floor(mouseY / unitLength);

    currentBoard[x][y] = 1;
    fill("#F0ACCC")  //currentBoard death change color
    stroke(strokeColor);
    rect(x * unitLength, y * unitLength, unitLength, unitLength);
}

function reset() {
    init()
    draw()
    noLoop()
}

function generateRandom() {
    for (let i = 0; i < columns; i++) {
        for (let j = 0; j < rows; j++) {
            currentBoard[i][j] = Math.random() > 0.5 ? 1 : 0;
            nextBoard[i][j] = 0;
        }
    }
    draw()
}

function windowResized() {
    resizeCanvas(windowWidth - settingWidth, windowHeight - settingHeight);
    columns = floor(width / unitLength);
    rows = floor(height / unitLength);

    let autoCatchWindowSize = []
    Object.assign(autoCatchWindowSize, currentBoard)
    init()
    try {
        for (let i = 0; i < columns; i++) {
            for (let j = 0; j < rows; j++) {
                if (autoCatchWindowSize[i][j] == 1) {
                    currentBoard[i][j] = autoCatchWindowSize[i][j]
                } else {
                    currentBoard[i][j] = 0
                }
            }
        }
    } catch(err) {
        console.error(err)
    }
}

// slider logic
var slider = document.getElementById("myRange");
var output = document.getElementById("current-frame-rate");
output.innerHTML = slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
    loop()
    fps = parseInt(this.value)
    output.innerHTML = fps
    frameRate(fps)
}

// setup button
document.getElementById('start').addEventListener('click',() => {
    loop()
    windowResized()
})
document.getElementById('next').addEventListener('click',() => nextFrame())
document.getElementById('stop').addEventListener('click',() => noLoop())
document.getElementById('reset').addEventListener('click',() => reset())
document.getElementById('generate-random').addEventListener('click',() => generateRandom())

// setup default input value
document.getElementById('loneliness-input').value = Loneliness
document.getElementById('overpopulation-input').value = Overpopulation
document.getElementById('reproduction-input').value = Reproduction


// setup update button
document.getElementById('loneliness-confirm-button').addEventListener('click',() => {
    Loneliness = document.getElementById('loneliness-input').value
    loop()
})
document.getElementById('overpopulation-confirm-button').addEventListener('click',() => {
    Overpopulation = document.getElementById('overpopulation-input').value
    loop()
})
document.getElementById('reproduction-confirm-button').addEventListener('click',() => {
    Reproduction = document.getElementById('reproduction-input').value
    loop()
})
